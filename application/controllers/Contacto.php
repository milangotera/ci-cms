<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'core/My_Controller.php';

class Contacto extends My_Controller {

	public function index()
	{
		if($this->input->method() == "post"){

			$name_contact     = $this->input->post('name_contact');
			$lastname_contact    = $this->input->post('lastname_contact');
			$email_contact    = $this->input->post('email_contact');
			$phone_contact   = $this->input->post('phone_contact');
			$message_contact = $this->input->post('message_contact');
			$verify_contact   = $this->input->post('verify_contact');

			$address = EMAIL_CONTACT;
			$e_subject = 'Has sido contactado por ' . $name_contact . '.';
			$e_body = "Usted ha sido contactado por $name_contact $lastname_contact con un mensaje adicional como sigue:";
			$e_content = $message_contact;
			$e_reply = "Puede ponerse en contacto con $lastname_contact via email, $email_contact o por teléfono $phone_contact";

			$this->form_validation->set_rules('name_contact', 'el nombre', 'required', ['required' => '%s es requerido.']);
			$this->form_validation->set_rules('lastname_contact', 'el apellido', 'required', ['required' => '%s es requerido.']);
			$this->form_validation->set_rules('email_contact', 'el email', 'required', ['required' => '%s es requerido.']);
			$this->form_validation->set_rules('phone_contact', 'el telefono', 'required', ['required' => '%s es requerido.']);
			$this->form_validation->set_rules('message_contact', 'el mensaje', 'required', ['required' => '%s es requerido.']);
			$this->form_validation->set_rules('verify_contact', 'verificar', 'required', ['required' => '%s es requerido.']);
	                
	        if($this->form_validation->run()){
				$from = $email_contact;
			    $to = $address;
			    $subject = $e_subject;
			    $messages  = "<html><head><title>".$e_subject."</title></head><body>";
			    $messages .= $e_body."<br><br>";
			    $messages .= $e_content."<br><br>";
			    $messages .= $e_reply."<br><br>";
			    $messages .= "</body></html>";
			    $headers = "From: ".$from." \r\n";
				$headers .= "Reply-To: ".$from."\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
				//var_dump($headers); exit();
				if(mail($to,$subject,$messages, $headers)) {
					$this->session->set_flashdata('mensaje', ["class"=>"success","text"=>"Su mensaje ha sido enviado. Nos pondremos en contacto con usted en breve."]);
				}else {
					$this->session->set_flashdata('mensaje', ["class"=>"danger","text"=>"no se pudo enviar su mensaje."]);
				}
			}

		}
		$this->data['menu'] = 0;
		$this->load_front(
			'front/home/contacto'
		);
	}
}
