CREATE OR REPLACE VIEW entrada_etiqueta AS
SELECT
C.id,
C.nombre,
C.seo,
E.id AS entrada
FROM etiqueta C
LEFT JOIN entrada_taxonomia T ON C.id = T.taxonomia AND T.tipo = 'etiqueta'
LEFT JOIN entrada E ON T.entrada = E.id;

CREATE OR REPLACE VIEW entrada_categoria AS
SELECT
C.id,
C.nombre,
C.seo,
E.id AS entrada
FROM categoria C
LEFT JOIN entrada_taxonomia T ON C.id = T.taxonomia AND T.tipo = 'categoria'
LEFT JOIN entrada E ON T.entrada = E.id;