<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entradas_Model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    public function list(
        $table = null,
        $column = null,
        $start = null,
        $length = null,
        $where = null,
        $order = null
    ){
        $select     = $column ? $column : '*';
        $table      = $table ? $table : 'entrada';
        $start      = $start ? $start : 0;
        $length     = $length ? $length : 0;
        $order = $order ? $order : ['column'=>'id', 'dir'=>'asc'];
        if($where){
            $this->db->where($where);
        }
        $this->db->select($select);
        $this->db->order_by($order['column'], $order['dir']);
        $consulta   = $this->db->get($table,$length,$start);
        $resultado  = $consulta->result();
        $total      = $consulta->num_rows();
        $data = [
          'total' => $total,
          'data' => $resultado
        ];
        return $data;
    }

    function list_public($limit = null, $offset = 0, $q = null){
        $this->db->select('entrada.*,user.username,user.nombre,user.apellido,user.avatar');
        $this->db->join('user', 'user.id = entrada.autor');
        if ($q != null) {
            $this->db->like('titulo', $q);
        }
        $this->db->where('entrada.estatus',1);
        $this->db->where('entrada.tipo',1);
        $this->db->limit($limit, $offset);
        $this->db->order_by('fecha', 'desc');
        $this->db->order_by('createdat', 'desc');
        $query = $this->db->get('entrada');

        return $query->result_array();
    }

    function list_categorias($seo, $limit = null, $offset = 0){
        $this->db->select('e.*,u.username,u.nombre,u.apellido,u.avatar');
        $this->db->join('categoria c','t.taxonomia=c.id');
        $this->db->join('entrada e','t.entrada=e.id');
        $this->db->join('user u','e.autor=u.id');
        $this->db->where('e.estatus',1);
        $this->db->where('c.seo',$seo);
        $this->db->where('t.tipo','categoria');
        $this->db->group_by('t.id');
        $this->db->order_by('e.fecha','desc');
        $this->db->order_by('e.createdat', 'desc');
        $posts = $this->db->get('entrada_taxonomia t',$limit,$offset)->result_array();
        return $posts;
    }

    function list_seo($seo){
        $this->db->select('entrada.*,user.username,user.nombre,user.apellido,user.avatar');
        $this->db->join('user', 'user.id = entrada.autor');
        $this->db->where('seo', $seo);
        $this->db->where('entrada.estatus',1);
        $this->db->where('entrada.tipo',1);
        $this->db->order_by('fecha', 'desc');
        $query = $this->db->get('entrada');
        return $query->result_array();
    }

    public function count($where = null)
    {
        if($where){
          $this->db->where($where);
        }
        $consult = $this->db->get('entrada');
        $total = $consult->num_rows();
        return $total;
    }

    public function insert($data = null)
    {
        $this->db->insert('entrada', $data);
        return $this->db->insert_id();
    }

    public function categorias($where = null)
    {
        if($where){
            $this->db->where($where);
        }
        $this->db->select('*');
        $consulta   = $this->db->get('entrada_categoria');
        $resultado  = $consulta->result();
        $total      = $consulta->num_rows();
        $data = [
          'total' => $total,
          'data' => $resultado
        ];
        return $data;
    }

    public function etiquetas($where = null)
    {
        if($where){
            $this->db->where($where);
        }
        $this->db->select('*');
        $consulta   = $this->db->get('entrada_etiqueta');
        $resultado  = $consulta->result();
        $total      = $consulta->num_rows();
        $data = [
          'total' => $total,
          'data' => $resultado
        ];
        return $data;
    }

    public function taxonomias($where = null)
    {
        if($where){
            $this->db->where($where);
        }
        $this->db->select('*');
        $consulta   = $this->db->get('entrada_taxonomia');
        $resultado  = $consulta->result();
        $total      = $consulta->num_rows();
        $data = [
          'total' => $total,
          'data' => $resultado
        ];
        return $data;
    }

    public function add_taxonomia($data = null)
    {
        $this->db->insert('entrada_taxonomia', $data);
        return $this->db->insert_id();
    }

    public function rem_taxonomia($where = null)
    {
        if($where){
          $this->db->where($where);
        }
        $this->db->delete('entrada_taxonomia');
        return true;
    }

    public function update($data = null, $where = null)
    {
        if($where){
          $this->db->where($where);
        }
        $this->db->update('entrada', $data);
        return true;
    }

    public function delete($where)
    {
        if($where){
          $this->db->where($where);
        }
        $this->db->delete('entrada');
        return true;
    }

}
