    <header class="header_sticky">  
        <a href="#menu" class="btn_mobile">
            <div class="hamburger hamburger--spin" id="hamburger" style="margin-top: 8px;">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
        </a>
        <!-- /btn_mobile-->
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-6">
                    <div id="logo">
                        <a href="<?=base_url();?>" title="Findoctor"><img src="<?=base_url();?>public/theme/front/<?=THEME_FRONT;?>/img/logo_w3ya.png" data-retina="true" alt="" width="50" height="50"></a>
                    </div>
                </div>
                <div class="col-lg-8 col-6">
                    <ul id="top_access">
                        <?php if($login['signin']){ ?>
                        <?php if($login['roles'] != 'suscriptor'){ ?>
                        
                        <?php } ?>
                        <li id="user">
                            <a href="<?=base_url();?>auth/perfil">
                                <figure><img src="<?=base_url();?><?=$login['avatar'];?>" alt=""></figure>
                                <?=$login['nombre'];?> <?=$login['apellido'];?>
                            </a>
                        </li>
                        <?php }else{ ?>
                        <li><a href="<?=base_url();?>auth/login"><i class="pe-7s-user"></i></a></li>
                        <li><a href="<?=base_url();?>auth/signup"><i class="pe-7s-add-user"></i></a></li>
                        <?php } ?>
                        
                    </ul>
                    <nav id="menu" class="main-menu" style="float: left;">
                        <ul>
                            <?php if($menu_itens['total']){ ?>
                            <?php foreach($menu_itens['data'] as $data){ ?>
                            <li>
                                <span>
                                    <a target="<?php echo $data->target; ?>" href="<?=base_url();?><?php echo $data->path; ?>">
                                        <span class="<?php echo $data->icono; ?>" style="display: none;"></span> <?php echo $data->nombre; ?>
                                    </a>
                                </span>
                            </li>
                            <?php }} ?>
                        </ul>
                    </nav>
                    <!-- /main-menu -->
                </div>
            </div>
        </div>
        <!-- /container -->
    </header>
    <!-- /header -->