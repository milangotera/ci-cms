<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Usuarios</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/usuarios/nuevo"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nuevo</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <?php if($this->session->flashdata('mensaje')) {
                          $message = $this->session->flashdata('mensaje');
                        ?>
                        <div class="alert alert-<?php echo $message['class']; ?>">
                            <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                            <?php echo $message['text']; ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <?php } ?>
                        <?php if(!count($usuarios['data'])){ ?>
                            <div class="alert alert-danger">
                                <span class="badge badge-pill badge-danger">danger</span>
                                No existen los datos que buscas.
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        <?php } else { ?>
                        <div class="table-responsive">
                            <table class="table table-bordered table-sm ">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Nombre</th>
                                        <th>Pais</th>
                                        <th>Grupo</th>
                                        <th>Estatus</th>
                                        <th>Accion</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if($usuarios['total']){ ?>
                                    <?php foreach($usuarios['data'] as $usuario){ ?>
                                    <?php $estatus =  $usuario->estatus ? "Activo" : "Inactivo"; ?>
                                    <tr>
                                    <td><?php echo $usuario->id; ?></td>
                                    <td class="titulo"><?php echo $usuario->username; ?></td>
                                    <td><?php echo $usuario->email; ?></td>
                                    <td><?php echo $usuario->nombre; ?> <?php echo $usuario->apellido; ?></td>
                                    <td><?php echo $usuario->pais; ?></td>
                                    <td><?php echo $usuario->roles; ?></td>
                                    <td><?php echo $estatus; ?></td>
                                    <td class="accion">
                                        <a type="button" class="btn btn-primary btn-sm" href="<?=base_url();?>auth/usuarios/editar/<?php echo $usuario->id; ?>"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        <a type="button" class="btn btn-danger btn-sm" href="<?=base_url();?>auth/usuarios/borrar/<?php echo $usuario->id; ?>"><i class="fa fa-times" aria-hidden="true"></i></a>
                                    </td>
                                    </tr>
                                    <?php }} ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="card-footer">
                        
                        <div class="row">
                            <!--<div class="col-sm-12"><center>Mostrando p&aacute;gina 1 de 1</center></div>-->
                            <nav class="pagination-center">
                                <ul class="pagination">
                                    <li class="page-item">
                                        <a class="page-link" href="<?=base_url();?>auth/usuarios/page/<?=($paginate['active']-1);?>/">
                                            <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> 
                                            <span class="hide-736">Anterior</span>
                                        </a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="#"><span class="hide-736">Mostrando p&aacute;gina</span> <?=($paginate['active']);?> de <?=($paginate['pages']);?></a>
                                    </li>
                                    <li class="page-item">
                                        <a class="page-link" href="<?=base_url();?>auth/usuarios/page/<?=($paginate['active']+1);?>/">
                                            <span class="hide-736">Siguiente</span> 
                                            <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                        </a>
                                    </li>
                                </ul>
                                <div style="clear: both;"></div>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
</div>