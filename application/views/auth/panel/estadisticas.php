    <section class="about py-lg-5 py-md-5 py-3">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-6 mg-t-10">
                    <div class="card bg-gray">
                        <div class="card-body">
                            <div class="w-30 pd-10">
                                <i class="fa fa-newspaper-o tx-50" aria-hidden="true"></i>
                            </div>
                            <div class="w-70 tx-20 pd-10">
                                <div class="stat-text"><b>Entradas</b></div>
                                <div class="stat-heading"><?=$entradas;?></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mg-t-10">
                    <div class="card bg-gray">
                        <div class="card-body">
                            <div class="w-30 pd-10">
                                <i class="fa fa-user-o tx-50" aria-hidden="true"></i>
                            </div>
                            <div class="w-70 tx-20 pd-10">
                                <div class="stat-text"><b>Usuarios</b></div>
                                <div class="stat-heading"><?=$usuarios;?></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mg-t-10">
                    <div class="card bg-gray">
                        <div class="card-body">
                            <div class="w-30 pd-10">
                                <i class="fa fa-envelope-o tx-50" aria-hidden="true"></i>
                            </div>
                            <div class="w-70 tx-20 pd-10">
                                <div class="stat-text"><b>Comentarios</b></div>
                                <div class="stat-heading"><?=$comentarios;?></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mg-t-10">
                    <div class="card bg-gray">
                        <div class="card-body">
                            <div class="w-30 pd-10">
                                <i class="fa fa-paper-plane-o tx-50" aria-hidden="true"></i>
                            </div>
                            <div class="w-70 tx-20 pd-10">
                                <div class="stat-text"><b>Newsletter</b></div>
                                <div class="stat-heading"><?=$newsletter;?></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section class="about">
        <div class="container">
            <div class="row">
                <?php if(count($mensajes)){ ?>
                    <?php foreach($mensajes as $mensaje){ ?>
                    <div class="col-sm-12">
                    <div class="alert alert-<?=$mensaje[0];?>">
                        <span class="badge badge-pill badge-<?=$mensaje[0];?>"><?=$mensaje[0];?></span>
                        <?=$mensaje[1];?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </section>

