<form method="post" action="<?=base_url();?>auth/galerias/nueva" enctype="multipart/form-data">

<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Nueva Galer&iacute;a</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/galerias"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive  pd-12">
                                
                                <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>

                                <?php if($this->session->flashdata('mensaje')) {
                                  $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                               
                                <div class="form-group">
                                    <label for="titulo">Nombre</label>
                                    <input type="text" name="nombre" class="form-control campo" id="nombre" placeholder="Nombre" value="<?php echo set_value('nombre'); ?>">
                                </div>

                                <div class="form-group pd-12 bg-light bd-1">
                                    <label for="foto" style="width: 100%; float: left;">Foto principal</label>
                                    <input type="file" name="foto" class="form-control-file campo" id="foto" placeholder="Foto principal" value="">
                                    <img id="blah" src="#" alt="Vista Previa" style="width: 300px; float: left; display: none; cursor: pointer;" title="Cambiar foto" />
                                    <dir style="clear: both;"></dir>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Tama&ntilde;os</label>
                                    <select name="tamano"  id="tamano" class="form-control select" value="<?php echo set_value('estatus'); ?>">
                                        <option value="800x400">Horizontal (800x400)</option>
                                        <option value="400x800">Vertical   (400x800)</option>
                                        <option value="800x800">Parejo     (800x800)</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Estatus</label>
                                    <select name="estatus"  id="estatus" class="form-control select" value="<?php echo set_value('estatus'); ?>">
                                        <option value="1">Activo</option>
                                        <option value="0">Inicativo</option>
                                    </select>
                                </div>

                        </div>
                    </div>
                    <div class="card-footer pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/galerias"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <br>
    <br>
    <br>
</div>

</form>

<script>
    $(document).ready(function() {

        $("#nombre").keyup(function(){
            mySeoURL("#nombre","#seo");
        });

        $('form').attr('autocomplete', "off");

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
                $("#blah").show();
            }
        }

        $("#foto").change(function(){
            readURL(this);
            $("#foto").hide();
        });

        $("#blah").click(function(){
            $("#foto").show();
            $("#blah").hide();
            $('#foto').val('');
        });

     });
</script>