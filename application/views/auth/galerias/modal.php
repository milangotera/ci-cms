<style type="text/css">
.item {
    float: left;
    position: relative;
    line-height: 1em;
}

.grid-sizer { width: 20%; }
.item { width: 20%; }

.item, .image { padding: 5px; }
.image { border: solid 1px #ccc; }

@media screen and (max-width: 1224px) {
  /* 10 columns for larger screens */
  .grid-sizer { width: 25%; }
  .item { width: 25%; }
}

@media screen and (max-width: 720px) {
  /* 10 columns for larger screens */
  .grid-sizer { width: 33.33%; }
  .item { width: 33.33%; }
}

@media screen and (max-width: 480px) {
  /* 10 columns for larger screens */
  .grid-sizer { width: 50%; }
  .item { width: 50%; }
}

.image{
    max-width: 100%;
    margin: 0;
    display: block;
}

.image:after {
    clear:both;
}

.onhandover {
    width: 100%;
    height: 100%;
    background-color: rgba(0,0,0,0.5);
    position: absolute;
    top: 0;
    left: 0;
    text-decoration: none;
    color: #fff;
    display: none;
}

.onhandover .title, .onhandover .description{
    color: #fff;
}

.onhandover .title {
    text-align: right;
    font-size: 30px;
}

.onhandover .title button{
    padding: 8px;
    padding-right: 12px;
    color: #fff;
}

.onhandover .description {
    position: absolute;
    bottom: 0;
    left: 0;
    background-color: rgba(0,0,0,0.80);
    width: 100%;
    margin: 0;
    padding: 5px !important;
    font-size: 12px;
}

.onhandover .description p {
    text-align: center;
}

.item:hover .onhandover {
    display: block;
}
</style>
<script src="<?=base_url();?>public/theme/<?=THEME_ASSETS;?>/library/masonry/masonry.pkgd.min.js"></script>
<script type="text/javascript">
$(window).load( function() {

    $("#masonry").masonry({
        "itemSelector": ".item",
        "columnWidth": ".grid-sizer",
    });

});
</script>
<div class="col-12 pd-12">
    <?php if($this->session->flashdata('mensaje')) {
      $message = $this->session->flashdata('mensaje');
    ?>
    <div class="alert alert-<?php echo $message['class']; ?>">
        <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
        <?php echo $message['text']; ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <?php } ?>
    <?php if(!count($galerias['data'])){ ?>
        <div class="alert alert-danger">
            <span class="badge badge-pill badge-danger">danger</span>
            No existen los datos que buscas.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
    <?php } ?>
    <div id="masonry">
        <div class="grid-sizer"></div>
        <?php if(count($galerias['data'])){ ?>
        <?php foreach($galerias['data'] as $galeria){ ?>
        <?php $estatus =  $galeria->estatus ? "Activo" : "Inactivo"; ?>
            <div class="item">
                <img src="<?=base_url();?><?=$galeria->path;?>" class="image"  alt="<?=$galeria->nombre;?>">
                <a class="onhandover seleccionar-galeria" href="#" title="Seleccionar Imagen" data-image="<?=$galeria->path;?>">
                    <h3 class="title">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="fa fa-toggle-off" aria-hidden="true"></i></span>
                        </button>
                    </h3>
                    <div class="description"><?=$galeria->nombre;?></div>
                </a>
            </div>
        <?php }} ?>
    </div>
    <div style="clear: both;"></div>
</div>
                                           
<div class="col-12 pd-12">
    <nav class="pagination-center">
        <ul class="pagination">
            <?php if($paginate['pages'] > 1){ ?>
            <?php if(($paginate['active']-1) > 0){ ?>
            <li class="page-item">
                <a class="page-link cargar-galeria" href="#" data-page="<?=($paginate['active']-1);?>">
                    <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> 
                    <span class="hide-736">Anterior</span>
                </a>
            </li>
            <?php } ?>
            <li class="page-item">
                <a class="page-link" href="#"><span class="hide-736">P&aacute;gina</span> <?=($paginate['active']);?> de <?=($paginate['pages']);?></a>
            </li>
            <?php if(($paginate['active']+1) <= $paginate['active']){ ?>
            <li class="page-item">
                <a class="page-link cargar-galeria" href="#" data-page="<?=($paginate['active']+1);?>">
                    <span class="hide-736">Siguiente</span> 
                    <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                </a>
            </li>
            <?php } ?>
            <?php } ?>
        </ul>
        <div style="clear: both;"></div>
    </nav>
</div>
<div class="hide" id="base_url"><?=base_url();?></div>
<script>
    $(document).ready(function(e) {
        $(".cargar-galeria").click(function(e){
            var page = ($(this).data('page'));
            $("#modal-body").load($("#base_url").html()+'auth/galerias/page/'+page+'/modal');
        });
        $(".seleccionar-galeria").click(function(e){
            var image = ($(this).data('image'));
            $('#image').attr('src', ($("#base_url").html()+image));
            $('#foto').val(image);
            $('#modal-galeria').modal('hide');
        });
    });
</script>
