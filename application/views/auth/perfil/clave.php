<?php if($usuarios['total']){ ?>
<?php foreach($usuarios['data'] as $usuario){ ?>
<?php $estatus =  $usuario->estatus ? 1 : 0; ?>

<form action="<?=base_url();?>auth/perfil/clave" method="post" autocomplete="off">

<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Cambiar Clave</button>
                            </div>
                            <div class="col-sm-6">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/perfil"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Editar Perfil</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive  pd-12">
                                
                                <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>

                                <?php if($this->session->flashdata('mensaje')) {
                                  $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                               
                                <div class="form-group">
                                    <label for="titulo">Clave actual</label>
                                    <input type="password" name="password" class="form-control campo" id="password" placeholder="Clave actual" value="">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Nueva clave</label>
                                    <input type="password" name="clave" class="form-control campo" id="clave" placeholder="Nueva clave" value="">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Repite clave</label>
                                    <input type="password" name="claver" class="form-control campo" id="claver" placeholder="Repite clave" value="">
                                </div>

                        </div>
                    </div>
                    <div class="card-footer pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <br>
    <br>
    <br>
</div>

</form>

<?php }} ?>
<script>
    $(document).ready(function() {

        $("#nombre").keyup(function(){
            mySeoURL("#nombre","#seo");
        });

        $('form').attr('autocomplete', "off");

     });
</script>