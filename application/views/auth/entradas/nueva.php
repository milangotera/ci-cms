<form method="post" action="<?=base_url();?>auth/entradas/nueva">

<div class="album py-5 bg-light">
    <div class="container">

        <div class="row">

            <div class="col-12">
                <div class="card card-default">
                    <div class="card-header pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button class="btn btn-titulo"><i class="fa fa-angle-right" aria-hidden="true"></i> Nueva Entrada</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/entradas"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive  pd-12">

                                <?php if(validation_errors()){ ?>
                                <div class="alert alert-danger">
                                    <?php echo validation_errors('<p class="error">','</p>'); ?>
                                </div>
                                <?php } ?>

                                <?php if($this->session->flashdata('mensaje')) {
                                  $message = $this->session->flashdata('mensaje');
                                ?>
                                <div class="alert alert-<?php echo $message['class']; ?>">
                                    <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                    <?php echo $message['text']; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php } ?>
                                                            
                                <div class="form-group">
                                    <label for="titulo">T&iacute;tulo</label>
                                    <input type="text" name="titulo" class="form-control campo" id="titulo" placeholder="T&iacute;tulo" value="<?php echo set_value('titulo'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="titulo">Seo</label>
                                    <input type="text" name="seo" class="form-control campo" id="seo" placeholder="Seo url" value="<?php echo set_value('seo'); ?>">
                                </div>

                                <div class="form-group">
                                    <label for="contenido">Contenido</label>
                                    <textarea name="contenido" id="contenido"><?php echo set_value('contenido'); ?></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="resumen">Resumen</label>
                                    <textarea name="resumen" class="form-control textarea" id="resumen"><?php echo set_value('resumen'); ?></textarea>
                                </div>

                                <div class="form-group pd-12 bg-light bd-1">
                                    <label for="foto" style="width: 100%; float: left;">Foto principal</label>
                                    <input type="hidden" name="foto" class="form-control-file campo" id="foto" value="<?php echo set_value('foto'); ?>">
                                    <button type="button" class="btn btn-titulo" data-toggle="modal" data-target="#modal-galeria" title="Cambiar Foto" style="border: solid 1px #ccc;">
                                    <img id="image" src="<?php echo $previa; ?>" alt="Vista Previa" style="width: 300px; float: left; cursor: pointer;" title="Cambiar foto" />
                                    </button>
                                    <dir style="clear: both;"></dir>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Autor</label>
                                    <select name="autor"  id="autor" class="form-control select">
                                        <?php if($autor['total']){ ?>
                                        <?php foreach($autor['data'] as $data){ ?>
                                        <option value="<?php echo $data->id; ?>" <?php if($login['id']==$data->id){ ?>selected<?php } ?>><?php echo $data->nombre." ".$data->apellido; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Fecha de Publicaci&oacute;n</label>
                                    <input type="date" name="fecha" class="form-control campo" id="fecha" placeholder="Fecha de Publicaci&oacute;n" value="<?php echo $fecha; ?>">
                                </div>

                                <div class="form-group">
                                    <label for="foto">Categor&iacute;as</label>
                                    <select name="categoria"  id="categoria" class="form-control select select2" multiple="multiple">
                                        <?php if($categorias['total']){ ?>
                                        <?php foreach($categorias['data'] as $data){ ?>
                                        <option value="<?php echo $data->id; ?>"><?php echo $data->nombre; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Etiquetas</label>
                                    <select name="etiqueta[]"  id="etiqueta" class="form-control select select2" multiple="multiple">
                                        <?php if($etiquetas['total']){ ?>
                                        <?php foreach($etiquetas['data'] as $data){ ?>
                                        <option value="<?php echo $data->id; ?>"><?php echo $data->nombre; ?></option>
                                        <?php }} ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Comentarios</label>
                                    <select name="comentarios"  id="comentarios" class="form-control select">
                                        <option value="1">Activar</option>
                                        <option value="0">Desactivar</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Tipo</label>
                                    <select name="tipo"  id="tipo" class="form-control select">
                                        <option value="1">Entrada</option>
                                        <option value="2">P&aacute;gina</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="foto">Estatus</label>
                                    <select name="estatus"  id="estatus" class="form-control select">
                                        <option value="2">Borrador</option>
                                        <option value="1">Publicar</option>
                                    </select>
                                </div>

                        </div>
                    </div>
                    <div class="card-footer pd-12">
                        <div class="row">
                            <div class="col-sm-8">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save" aria-hidden="true"></i> Guardar</button>
                            </div>
                            <div class="col-sm-4">
                                <a type="button" class="btn btn-default float-right-md" href="<?=base_url();?>auth/entradas"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i> Volver</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <br>
    <br>
    <br>
</div>

</form>

<!-- Modal -->
<div class="modal fade" id="modal-galeria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Galer&iacute;a de Im&aacute;genes</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body"></div>
      <div class="modal-footer hide">
        <a type="button" class="btn btn-default float-right-md nueva-galeria" href="#"><i class="fa fa-plus-circle" aria-hidden="true"></i> Nueva</a>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function(e) {
        $('#contenido').summernote();

        $("#titulo").keyup(function(e){
            mySeoURL("#titulo","#seo");
        });
    
        $('.select2').select2();

        $('#modal-galeria').on('show.bs.modal', function (e) {
            $("#modal-body").load($("#base_url").html()+'auth/galerias/page/1/modal');
        });

        $('#modal-galeria').on('hide.bs.modal', function (e) {
            $("#modal-body").html('');
        });

    });
</script>