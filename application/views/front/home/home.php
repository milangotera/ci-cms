<div class="hero_home version_3 bg-off">
    <div class="content">
        <h3 class="fadeInUp animated">Empieza Hoy!</h3>
        <p class="fadeInUp animated">
            Aprende a programar de una buena vez de la forma más sencilla que viste en toda tu vida.
        </p>
        <a href="entradas" class="btn_1 medium fadeIn animated">Empezar Ya!</a>
    </div>
</div>

<div class="container margin_120_95">
<div class="main_title">
    <h2>La&nbsp;<strong>creatividad</strong> no tiene límites!</h2>
    <p>La magia comienza cuando la eficiencia de las herramientas y la creatividad de la mente humana se combinan.</p>
</div>
<div class="row add_bottom_30">
    <div class="col-lg-4">
        <div class="box_feat" id="icon_1">
            <span></span>
            <h3>Artículos</h3>
            <p style="text-align: justify;">Disfruta de una publicaci&oacute;n regular de contenido interesante acerca de computación en la nube, nuevas tendencias y más.</p>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box_feat" id="icon_2">
            <span></span>
            <h3>Tutoriales</h3>
            <p style="text-align: justify;">Aprende a programar y realizar trucos interesantes de la forma más sencilla posible que pudiste imaginar en toda tu vida.</p>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="box_feat" id="icon_3">
            <h3>Recursos</h3>
            <p style="text-align: justify;">Descarga contenido, guías, y herramientas que de seguro te serán de gran utilidad como consumidor regular del sitio.</p>
        </div>
    </div>
</div>
</div>