<style type="text/css">
	.menu_categoria{
		width: 100%;
		color: #fff !important;
	}
	.menu_categoria span{
		float: left;
	}
	.menu_categoria i{
		float: right;
	}
	.ul_categoria{
		width: 100%;
		border-radius: 0px;
		background: #0064a2;
		color: #fff;
		padding: 0px;
	}
	.ul_categoria li a{
		color: #fff;
		padding-left: 12px;
	}
	.ul_categoria li a:hover{
		color: #0064a2;
	}
</style>
<section class="about" style="padding-top: 30px; position: relative; z-index: 10;">
	<section class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-4 col-sm-12">
					<div class="dropdown">
						<a class="btn menu_categoria dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<span>Categor&iacute;as</span> <i class="fa fa-chevron-down" aria-hidden="true"></i>
						</a>
						<ul class="dropdown-menu dropdown-breadcrumbs ul_categoria"  aria-labelledby="dropdownMenuButton">
							<?php if($breadcrumbs_categorias['total']){ ?>
			                <?php foreach($breadcrumbs_categorias['data'] as $data){ ?>
							<li><a class="dropdown-item" href="<?=base_url();?>categorias/<?php echo $data->seo; ?>"><?php echo $data->nombre; ?></a></li>
							<?php }} ?>
						</ul>
					</div>
				</div>
				<div class="col-lg-6 col-md-4 col-sm-12"></div>
				<div class="col-lg-3 col-md-4 col-sm-12">
					<div class="row no-gutters" style="margin: 0px;">
				        <div class="col">
				            <input class="form-control border-secondary border-right-0 rounded-0" type="search" value="Buscar">
				        </div>
				        <div class="col-auto">
				            <button class="btn btn-outline-secondary border-left-0 rounded-0 rounded-right" type="button">
				                <i class="fa fa-search"></i>
				            </button>
				        </div>
				    </div>
			    </div>
		    </div>
	    </div>
	</section>
</section>