                            <hr>
                            <?php if($login['signin']){ ?>
                            <div class="comment-top contact">
                                <h4>Deja un comentario</h4>
                                <form name="contactform" id="contactform" method="post" action="" autocomplete="off">
                                    <?php if(validation_errors()){ ?>
                                    <div class="alert alert-danger">
                                        <?php echo validation_errors('<p class="error">','</p>'); ?>
                                    </div>
                                    <?php } ?>
                                    <?php if($this->session->flashdata('mensaje')) {
                                      $message = $this->session->flashdata('mensaje');
                                    ?>
                                    <div class="alert alert-<?php echo $message['class']; ?>">
                                        <span class="badge badge-pill badge-<?php echo $message['class']; ?>"><?php echo $message['class']; ?></span>
                                        <?php echo $message['text']; ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <?php } ?>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group ">
                                                <label>Nombre</label>
                                                <input type="text" class="form-control" id="comentar_nombre" placeholder="Escribe tu Nombre" name="comentar_nombre" value="<?php echo $login['nombre']; ?> <?php echo $login['apellido']; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" class="form-control" id="comentar_email" placeholder="Escribe tu Email" name="comentar_email" value="<?php echo $login['email']; ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Web</label>
                                                <input type="text" class="form-control" id="comentar_web" placeholder="Tu perfil o p&aacute;gina" name="comentar_web" rows="6" value="<?php echo $login['web']; ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Enviar comentario</label>
                                        <textarea name="comentar_comentario" class="form-control input_comentar" id="comentar_comentario" placeholder="Escribe tu comentario"></textarea>
                                    </div>
                                    <button type="submit" class="btn_1">Enviar</button>
                                </form>
                            </div>
                            <?php }else{ ?>
                                <div class="comment-top">
                                <div class="alert alert-warning">
                                    Ingresa y d&eacute;janos tu comentario.
                                </div>
                                </div>
                            <?php } ?>